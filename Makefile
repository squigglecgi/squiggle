O ?= $(shell pwd)/bin

all: $(O)/squiggle

$(O)/squiggle: src/main.cpp $(O)/squirrel/lib/libsquirrel.a $(O)/squirrel/lib/libsqstdlib.a
	@echo "CC $@"; if [ ! -d "$(shell dirname $@)" ]; then mkdir -p "$(shell dirname $@)"; fi && $(CXX) -Wall -Werror -Iexternal/squirrel/include src/main.cpp "$(O)/squirrel/lib/libsquirrel.a" "$(O)/squirrel/lib/libsqstdlib.a" -o "$@"

$(O)/squirrel/lib/libsquirrel.a:
	+@echo "MAKE external/squirrel/squirrel"; if [ ! -d "$(shell dirname $@)" ]; then mkdir -p "$(shell dirname $@)"; fi && if [ ! -d "$(O)/squirrel/include" ]; then cp -r "$(shell pwd)/external/squirrel/include" "$(O)/squirrel"; fi && make SQUIRREL="$(O)/squirrel" -C external/squirrel/squirrel

$(O)/squirrel/lib/libsqstdlib.a:
	+@echo "MAKE external/squirrel/sqstdlib"; if [ ! -d "$(shell dirname $@)" ]; then mkdir -p "$(shell dirname $@)"; fi && if [ ! -d "$(O)/squirrel/include" ]; then cp -r "$(shell pwd)/external/squirrel/include" "$(O)/squirrel"; fi && make SQUIRREL="$(O)/squirrel" -C external/squirrel/sqstdlib