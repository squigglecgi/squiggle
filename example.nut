setHeader("X-Hello", "World")
status(200, "text/html")

print("<h1>Hello, Squiggle!</h1><p>This page was rendered by a CGI script written in Squirrel.</p>")
print("<p>Request method: " + getRequestMethod() + "</p>")
print("<p>Request path: " + getRequestPath() + "</p>")

if (getRequestParameter("example") == "" && getRequestParameter("example2") == "") {
	print("<p>Try setting example and/or example2 in the URL parameters. ;)</p>")
} else {
	print("<hr>")
}

if (getRequestParameter("example") != "") {
	print("example -> " + getRequestParameter("example") + "</p>")
}

if (getRequestParameter("example2") != "") {
	print("example2 -> " + getRequestParameter("example2") + "</p>")
}