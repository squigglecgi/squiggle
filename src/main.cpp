#include <squirrel.h>
#include <sqstdblob.h>
#include <sqstdsystem.h>
#include <sqstdio.h>
#include <sqstdmath.h>
#include <sqstdstring.h>
#include <sqstdaux.h>
#include <stdarg.h>
#include <cstdlib>
#include <cstring>
#include <stdio.h>
#include <sstream>
#include <string>
#include <map>

#ifdef SQUNICODE
#define scfprintf fwprintf
#define scvprintf vfwprintf
#else
#define scfprintf fprintf
#define scvprintf vfprintf
#endif

using namespace std;

map<string, string> headers;
map<string, string> parameters;

void printfunc(HSQUIRRELVM SQ_UNUSED_ARG(vm), const SQChar *s, ...) {
	va_list vl;
	va_start(vl, s);
	scvprintf(stdout, s, vl);
	va_end(vl);
	fprintf(stdout, "\n");
}

void errorfunc(HSQUIRRELVM SQ_UNUSED_ARG(vm), const SQChar *s, ...) {
	va_list vl;
	va_start(vl, s);
	scvprintf(stderr, s, vl);
	va_end(vl);
	fprintf(stderr, "\n");
}

static SQInteger get_request_method(HSQUIRRELVM vm) {
	char* request_method = getenv("REQUEST_METHOD");

	if (!request_method) {
		sq_throwerror(vm, _SC("REQUEST_METHOD is not set"));
		return SQ_ERROR;
	}

	sq_pushstring(vm, request_method, strlen(request_method));
	return 1;
}

static SQInteger get_request_path(HSQUIRRELVM vm) {
	char* document_uri = getenv("DOCUMENT_URI");

	if (!document_uri) {
		sq_throwerror(vm, _SC("DOCUMENT_URI is not set"));
		return SQ_ERROR;
	}

	sq_pushstring(vm, document_uri, strlen(document_uri));
	return 1;
}

static SQInteger get_request_parameter(HSQUIRRELVM vm) {
	const SQChar* key;

	if (SQ_FAILED(sq_getstring(vm, 2, &key))) {
		sq_throwerror(vm, _SC("Argument 1 is not a valid string"));
		return SQ_ERROR;
	}

	if (parameters.find(key) == parameters.end()) {
		sq_pushstring(vm, "", 0);
		return 1;
	}

	const char* value = parameters[key].c_str();

	sq_pushstring(vm, value, strlen(value));

	return 1;
}

static SQInteger set_header(HSQUIRRELVM vm) {
	const SQChar* key;
	const SQChar* value;

	if (SQ_FAILED(sq_getstring(vm, 2, &key))) {
		sq_throwerror(vm, _SC("Argument 1 is not a valid string"));
		return SQ_ERROR;
	}

	if (SQ_FAILED(sq_getstring(vm, 3, &value))) {
		sq_throwerror(vm, _SC("Argument 2 is not a valid string"));
		return SQ_ERROR;
	}

	headers[key] = value;

	return 0;
}

static SQInteger status(HSQUIRRELVM vm) {
	SQInteger status_code;

	if (SQ_FAILED(sq_getinteger(vm, 2, &status_code))) {
		sq_throwerror(vm, _SC("Argument 1 is not a valid integer"));
		return SQ_ERROR;
	}

	const SQChar* content_type;

	if (SQ_FAILED(sq_getstring(vm, 3, &content_type))) {
		sq_throwerror(vm, _SC("Argument 2 is not a valid string"));
		return SQ_ERROR;
	}

	map<string, string>::iterator it = headers.begin();

	while (it != headers.end()) {
		fprintf(stdout, "%s: %s\n", it->first.c_str(), it->second.c_str());
		++it;
	}

	switch (static_cast<int>(status_code)) {
		case 200:
			fprintf(stdout, "Status: 200 OK\n");
			break;
		case 401:
			fprintf(stdout, "Status: 401 Unauthorized\n");
			break;
		case 403:
			fprintf(stdout, "Status: 403 Forbidden\n");
			break;
		case 404:
			fprintf(stdout, "Status: 404 Not Found\n");
			break;
		case 501:
			fprintf(stdout, "Status: 501 Not Implemented\n");
			break;
		default:
			fprintf(stdout, "Status: 500 Internal Server Error\n");
	}

	fprintf(stdout, "Content-Type: %s\n\n", content_type);

	return 0;
}

static SQInteger log(HSQUIRRELVM vm) {
	const SQChar* type;
	const SQChar* msg;

	if (SQ_FAILED(sq_getstring(vm, 2, &type))) {
		sq_throwerror(vm, _SC("Argument 1 is not a valid string"));
		return SQ_ERROR;
	}

	if (SQ_FAILED(sq_getstring(vm, 3, &msg))) {
		sq_throwerror(vm, _SC("Argument 2 is not a valid string"));
		return SQ_ERROR;
	}

	fprintf(stderr, "[%s]: %s\n", type, msg);

	return 0;
}

int main() {
	//
	// Set up the Squirrel VM
	//

	HSQUIRRELVM vm = sq_open(1024);

	sq_setprintfunc(vm, printfunc, errorfunc);
	sq_pushroottable(vm);

	sqstd_register_iolib(vm);
	sqstd_register_systemlib(vm);
	sqstd_register_mathlib(vm);
	sqstd_register_stringlib(vm);

	//
	// Set up Squiggle-specific functions
	//

	sq_pushstring(vm, "getRequestMethod", -1);
	sq_newclosure(vm, &get_request_method, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register getRequestMethod()\n");
		return 1;
	}

	sq_pushstring(vm, "getRequestPath", -1);
	sq_newclosure(vm, &get_request_path, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register getRequestPath()\n");
		return 1;
	}

	sq_pushstring(vm, "getRequestParameter", -1);
	sq_newclosure(vm, &get_request_parameter, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".s");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register getURLParameters()\n");
		return 1;
	}

	sq_pushstring(vm, "setHeader", -1);
	sq_newclosure(vm, &set_header, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".ss");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register setHeader()\n");
		return 1;
	}

	sq_pushstring(vm, "status", -1);
	sq_newclosure(vm, &status, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".ns");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register status()\n");
		return 1;
	}

	sq_pushstring(vm, "log", -1);
	sq_newclosure(vm, &log, 0);
	sq_setparamscheck(vm, SQ_MATCHTYPEMASKSTRING, ".ss");

	if (SQ_FAILED(sq_createslot(vm, -3))) {
		fprintf(stderr, "Failed to register log()\n");
		return 1;
	}

	//
	// Set up headers
	//

	headers["X-Squiggle"] = "1";

	//
	// Set up parameters
	//

	char* query_string = getenv("QUERY_STRING");

	if (query_string) {
		std::string query_string_cppstr(query_string);
		stringstream query_string_stream(query_string_cppstr);
		string buffer;

		while (getline(query_string_stream, buffer, '&')) {
			stringstream parameter_stream(buffer);
			string buffer2;
			string key;
			string value;

			while (getline(parameter_stream, buffer2, '=')) {
				if (key.empty()) {
					key = buffer2;
				}

				value = buffer2;
			}

			parameters[key] = value;
		}
	}

	//
	// Run the Squirrel file
	//

	char* filename = getenv("SQUIGGLE_FILENAME");

	if (!filename) {
		fprintf(stderr, "SQUIGGLE_FILENAME is not set!\n");
		return 1;
	}

	if (SQ_FAILED(sqstd_dofile(vm, filename, 0, 1))) {
		fprintf(stderr, "Failed to run %s!\n", filename);
		return 1;
	}

	//
	// Close the Squirrel VM
	//

	sq_close(vm);
}
